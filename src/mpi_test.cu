#include "mpi.h"
#include <stdio.h>
#include "gpu_sha256.cuh"
#include "printf.cuh"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

extern "C" {
	#include "cpu_sha256.h"
}

#define MPI_TAG_DIFFICULTY 4
#define MPI_TAG_CAPABILITY 5
#define MPI_TAG_JOB 6
#define MPI_TAG_SOLUTION 7

#define NUM_BLOCKS 10

#define BLOCK_SIZE 85

static int DIFFICULTY = 20;

struct CapabilityReport
{
    char hostname[MPI_MAX_PROCESSOR_NAME];
	int gpuCount;
};

struct Job
{
	bool useGpu;
	unsigned long long startNonce;
	unsigned long long endNonce;
};

void reportCapabilities(int masterRank)
{
	struct CapabilityReport capReport = {0};
	cudaError_t err = cudaGetDeviceCount(&capReport.gpuCount);
	if (err != cudaSuccess) 
		printf("Cuda error: %s\n", cudaGetErrorString(err));
	else
		printf("Found %d cuda devices\n", capReport.gpuCount);

	int len;
	MPI_Get_processor_name(capReport.hostname, &len);

	// report capability to master node
	MPI_Send(&capReport, sizeof(struct CapabilityReport), MPI_BYTE, masterRank, MPI_TAG_CAPABILITY, MPI_COMM_WORLD);
}

void collectCapabilitiesAndDistributeWork(int masterRank, int numTasks)
{
	printf("I am the master\n");

	// the master will gather all the capabilities and distribute the work appropriately
	int totalGPUs = 0;
	int totalCPUs = 0;

	char **gpuHostNames = (char**)malloc(sizeof(char**) * numTasks);

	// collect the capabilities
	int *workerGPUs = (int*)malloc(sizeof(int) * numTasks);
	for (int i = 0; i < numTasks; i++)
	{
		struct CapabilityReport curReport;
		MPI_Status status;
		MPI_Recv(&curReport, sizeof(struct CapabilityReport), MPI_BYTE, i, MPI_TAG_CAPABILITY, MPI_COMM_WORLD, &status);

		printf("Worker %d has %d GPUs.\n", i, curReport.gpuCount);

		int gpusToUse;
		if (curReport.gpuCount >= 1)
		{
			// check if the host has already been reported, only one worker
			// per host will handle its GPU
			bool hostReported = false;
			for (int j = 0; j < numTasks; j++)
			{
				if (gpuHostNames[j] == NULL)
				{
					gpuHostNames[j] = (char*)malloc(strlen(curReport.hostname));
					strcpy(gpuHostNames[j], curReport.hostname);
					break;
				}
				else if (strcmp(curReport.hostname, gpuHostNames[j]) == 0)
				{
					hostReported = true;
					break;	
				}
			}

			if (hostReported)	
			{
				totalCPUs++;
				gpusToUse = 0;
			}
			else
			{
				totalGPUs++;
				gpusToUse = 1;
			}
		}
		else
		{
			totalCPUs++;
			gpusToUse = 0;
		}

		workerGPUs[i] = gpusToUse;;
	}


	free(gpuHostNames);

	int gpuWorkToCPUWorkRatio = 5; // each GPU gets 5x the amount of work that a CPU gets
	int workShareCount = totalGPUs * gpuWorkToCPUWorkRatio + totalCPUs;
	unsigned long long workShareSize = ULLONG_MAX / workShareCount;

	// distribute the work
	int totalSharesDistributed = 0;
	for (int curRank = 0; curRank < numTasks; curRank++)
	{
		// determine the amount of shares of work this worker is responsible for
		int curShareCount = (workerGPUs[curRank] > 0) ? gpuWorkToCPUWorkRatio : 1;

		struct Job job;
		job.useGpu = workerGPUs[curRank] > 0;
		job.startNonce = workShareSize * totalSharesDistributed;
		job.endNonce = workShareSize * (totalSharesDistributed + curShareCount);

		MPI_Send(&job, sizeof(struct Job), MPI_BYTE, curRank, MPI_TAG_JOB, MPI_COMM_WORLD);

		totalSharesDistributed += curShareCount;
	}

	free(workerGPUs);
}

void sha256HashToStr(char *hash, char *out) 
{
	for (int i = 0; i < SHA256_BLOCK_SIZE; i++) {
		sprintf(out + i * 2, "%02x", (unsigned char)hash[i]);
	}
}

void getBlockBufferCpu(unsigned long long nonce, char *out, char *prevBlockHash)
{
	sha256HashToStr(prevBlockHash, out);
	out[2 * SHA256_BLOCK_SIZE] = '|';
	sprintf(out + 2 * SHA256_BLOCK_SIZE + 1, "%llu", nonce);
}

__device__ void sha256HashToStrGpu(char *hash, char *out)
{
	for (int i = 0; i < SHA256_BLOCK_SIZE; i++) {
		simple_sprintf(out + i * 2, "%02x", (unsigned char)hash[i]);
	}
}

__device__ void getBlockBufferGpu(unsigned long long nonce, char *out, char *prevBlockHash)
{
	sha256HashToStrGpu(prevBlockHash, out);
	out[2 * SHA256_BLOCK_SIZE] = '|';
	simple_sprintf(out + 2 * SHA256_BLOCK_SIZE + 1, "%llu", nonce);
}

/*
void getBlockBufferCpu(unsigned long long nonce, char *out, char *prevBlockHash)
{
	memset(out, 0, BLOCK_SIZE);

	memcpy(out, prevBlockHash, SHA256_BLOCK_SIZE);
	out[SHA256_BLOCK_SIZE] = '|';
	memcpy(out + SHA256_BLOCK_SIZE + 1, &nonce, sizeof(unsigned long long));
}

__device__ void getBlockBufferGpu(unsigned long long nonce, char *out, char *prevBlockHash)
{
	memset(out, 0, BLOCK_SIZE);

	memcpy(out, prevBlockHash, SHA256_BLOCK_SIZE);
	out[SHA256_BLOCK_SIZE] = '|';
	memcpy(out + SHA256_BLOCK_SIZE + 1, &nonce, sizeof(unsigned long long));
}*/

void doubleSha256Cpu(const char *buffer, int length, char *hashResult)
{
	CPU_SHA256_CTX context1;
	BYTE hashBuffer[SHA256_BLOCK_SIZE];
	
	char hashStrBuffer[SHA256_BLOCK_SIZE * 2 + 2] = {0};

	cpu_sha256_init(&context1);
	cpu_sha256_update(&context1, (BYTE*)buffer, length);
	cpu_sha256_final(&context1, hashBuffer);

	sha256HashToStr((char*)hashBuffer, hashStrBuffer);

	CPU_SHA256_CTX context2;
	cpu_sha256_init(&context2);
	cpu_sha256_update(&context2, (BYTE*)hashStrBuffer, SHA256_BLOCK_SIZE * 2);
	cpu_sha256_final(&context2, (BYTE*)hashResult);
}

__device__ void doubleSha256Gpu(const char *buffer, int length, char *hashResult)
{
	GPU_SHA256_CTX context1;
	BYTE hashBuffer[SHA256_BLOCK_SIZE];

	char hashStrBuffer[SHA256_BLOCK_SIZE * 2 + 2] = {0};

	gpu_sha256_init(&context1);
	gpu_sha256_update(&context1, (BYTE*)buffer, length);
	gpu_sha256_final(&context1, hashBuffer);

	sha256HashToStrGpu((char*)hashBuffer, hashStrBuffer);

	GPU_SHA256_CTX context2;
	gpu_sha256_init(&context2);
	gpu_sha256_update(&context2, (BYTE*)hashStrBuffer, SHA256_BLOCK_SIZE * 2);
	gpu_sha256_final(&context2, (BYTE*)hashResult);
}

__device__ void isSolutionGpu(const char *hash, bool *isSolution, int difficulty)
{
	for (int i = 0; i < difficulty; i++)
	{
		int bitIndex = (SHA256_BLOCK_SIZE * 8 - i - 1) % 8;
		int byteIndex = i / 8;

		char bitMask = 1 << bitIndex;
		if (hash[byteIndex] & bitMask) 
		{
			*isSolution = false;
			return;
		}
	}

	*isSolution = true;
}

bool isSolution(const char *hash)
{
	for (int i = 0; i < DIFFICULTY; i++)
	{
		int bitIndex = (SHA256_BLOCK_SIZE * 8 - i) % 8;
		int byteIndex = i / 8;

		char bitMask = 1 << bitIndex;
		if (hash[byteIndex] & bitMask)
			return false;
	}

	return true;
}

int checkForSolutionFromOtherNodes(int blockIndex)
{
	int receivedSolution;
	MPI_Status status;

	MPI_Iprobe(MPI_ANY_SOURCE, MPI_TAG_SOLUTION + blockIndex, MPI_COMM_WORLD, &receivedSolution, &status);

	return receivedSolution;
}

unsigned long long getSolutionFromOtherNodes(int blockIndex) {
	MPI_Status status;
	unsigned long long solutionNonce;
	MPI_Recv(&solutionNonce, sizeof(unsigned long long), MPI_BYTE, 
				MPI_ANY_SOURCE, MPI_TAG_SOLUTION + blockIndex, MPI_COMM_WORLD, &status); 
	return solutionNonce;
}

void printSha256Hash(char *hashBuffer) {
	char hashStr[SHA256_BLOCK_SIZE * 2 + 2] = {0};
	sha256HashToStr(hashBuffer, hashStr);
	printf("%s\n", hashStr);
}

unsigned long long cpuFindSolution(unsigned long long start, unsigned long long end, int numTasks, char *prevBlockHash, int blockIndex)
{
	char hashBuffer[SHA256_BLOCK_SIZE] = {0};
	char blockBuffer[BLOCK_SIZE] = {0};
	for (unsigned long long curNonce = start; curNonce < end; curNonce++)
	{
		if (curNonce % 1000 == 0 && checkForSolutionFromOtherNodes(blockIndex))
		{
			printf("Got a solution from another node\n");

			// receive the solution nonce from the other nodes
			return getSolutionFromOtherNodes(blockIndex);
		}

		// try the current nonce value
		getBlockBufferCpu(curNonce, blockBuffer, prevBlockHash);
		doubleSha256Cpu(blockBuffer, strlen(blockBuffer), hashBuffer);

		if (isSolution(hashBuffer)) {
			printf("Found solution via CPU for block %d. Nonce = %llu hash = ", blockIndex, curNonce);
			printSha256Hash(hashBuffer);

			// notify all other workers of the solution
			MPI_Request request;
			for (int curRank = 0; curRank < numTasks; curRank++)
				MPI_Send(&curNonce, sizeof(unsigned long long), MPI_BYTE, curRank, MPI_TAG_SOLUTION + blockIndex, MPI_COMM_WORLD);
			printf("Finished sending CPU found solution for block %d.\n", blockIndex);
			return curNonce;
		}
	}

	return 0;
}

__device__ int gpuStrLen(char *str) {
	char *tmp = str;
	while (*tmp++);

	return tmp - str - 1;
}

__global__ void bitcoinKernel(unsigned long long startNonce, unsigned long long endNonce, unsigned long long *result, int difficulty, char *prevBlockHash)
{
	unsigned int globalThreadIndex = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int totalThreadCount = gridDim.x * blockDim.x;

	if (globalThreadIndex == 0 && startNonce == 0) 
		printf("Started first kernel\n");

	unsigned long long nonceRange = (endNonce - startNonce);
	unsigned long long myNonceStart = (globalThreadIndex * nonceRange) / totalThreadCount + startNonce;
	unsigned long long myNonceEnd = ((globalThreadIndex + 1) * nonceRange) / totalThreadCount + startNonce;

	char hashBuffer[SHA256_BLOCK_SIZE] = {0};
	bool isSolution = false;
	for (unsigned long long curNonce = myNonceStart; curNonce < myNonceEnd; curNonce++)
	{
		char blockBuffer[BLOCK_SIZE] = {0};
		getBlockBufferGpu(curNonce, blockBuffer, prevBlockHash);
		doubleSha256Gpu(blockBuffer, gpuStrLen(blockBuffer), hashBuffer);

		isSolutionGpu(hashBuffer, &isSolution, difficulty);
		if (isSolution)
		{
			char hashStrBuffer[SHA256_BLOCK_SIZE * 2 + 1] = {0};
			sha256HashToStrGpu(hashBuffer, hashStrBuffer);

			*result = curNonce;
			return;
		}
	}
}

unsigned long long gpuFindSolution(Job myJob, int numTasks, char *prevBlockHash, int blockIndex)
{
	printf("Using GPU\n");
	cudaSetDevice(2);

	unsigned long long curStartNonce = myJob.startNonce;
	unsigned long long gpuBatchSize = 16 * 1024 * 100;

	unsigned long long result;
	unsigned long long *d_result;
	cudaMalloc(&d_result, sizeof(unsigned long long));

	char *d_prevBlockHash;
	cudaMalloc(&d_prevBlockHash, SHA256_BLOCK_SIZE);

	while (curStartNonce < myJob.endNonce) 
	{
		cudaMemcpy(d_prevBlockHash, prevBlockHash, SHA256_BLOCK_SIZE, cudaMemcpyHostToDevice);
		bitcoinKernel<<<16,1024>>>(curStartNonce, curStartNonce + gpuBatchSize, d_result, DIFFICULTY, d_prevBlockHash);
		cudaMemcpy(&result, d_result, sizeof(unsigned long long), cudaMemcpyDeviceToHost);

		bool solutionFromOtherNodes = checkForSolutionFromOtherNodes(blockIndex);
		if (result != 0 || solutionFromOtherNodes)
		{
			printf("Found a solution, solutionFromOtherNodes = %d, result = %d\n", solutionFromOtherNodes, result);
			if (solutionFromOtherNodes)
			{
				printf("Got a solution from another node, stopping GPU.\n");
				return getSolutionFromOtherNodes(blockIndex);
			}
			else 
			{
				printf("Found solution via GPU for block %d\n", blockIndex);
				// notify all other workers of the solution
				for (int curRank = 0; curRank < numTasks; curRank++)
					MPI_Send(&result, sizeof(unsigned long long), MPI_BYTE, curRank, MPI_TAG_SOLUTION + blockIndex, MPI_COMM_WORLD);
				printf("Sent solution for block %d\n", blockIndex);
				return result;
			}
		}

		curStartNonce += gpuBatchSize;
	}

	cudaFree(d_result);
	return 0;
}

int main(int argc, char *argv[]) {
	int  numTasks, rank;
	// initialize MPI
	MPI_Init(&argc,&argv);

	// get number of tasks
	MPI_Comm_size(MPI_COMM_WORLD,&numTasks);

	// get my rank
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	printf("Rank = %d\n", rank);
	printf("Task Count = %d\n", numTasks);

	int masterRank = numTasks - 1;
	reportCapabilities(masterRank);

	if (rank == masterRank)
	{
		collectCapabilitiesAndDistributeWork(masterRank, numTasks);
	}

	MPI_Status status;
	struct Job myJob;
	MPI_Recv(&myJob, sizeof(struct Job), MPI_BYTE, masterRank, MPI_TAG_JOB, MPI_COMM_WORLD, &status);

	printf("Got my job. %llu - %llu\n", myJob.startNonce, myJob.endNonce);

	char prevBlockHash[SHA256_BLOCK_SIZE] = {0};
	const char *dataToHash = "CSCI-654 Foundations of Parallel Computing";
	doubleSha256Cpu(dataToHash, strlen(dataToHash), prevBlockHash);

	double avgBlockGenerationTime = 30;

	char blockBuffer[BLOCK_SIZE];
	for (int i = 0; i < NUM_BLOCKS; i++)
	{
		struct timespec startTime;
		clock_gettime(CLOCK_MONOTONIC, &startTime);

		printf("--> Worker %d solving for block %d\n", rank, i);
		unsigned long long nonce;
		if (myJob.useGpu)
		{
			nonce = gpuFindSolution(myJob, numTasks, prevBlockHash, i);
		}
		else 
		{
			printf("Using CPU\n");
			nonce = cpuFindSolution(myJob.startNonce, myJob.endNonce, numTasks, prevBlockHash, i);
		}

		getBlockBufferCpu(nonce, blockBuffer, prevBlockHash);
		doubleSha256Cpu(blockBuffer, strlen(blockBuffer), prevBlockHash);

		printf("Worker %d Block %d solution nonce: %llu Hash: ", rank, i, nonce);
		printSha256Hash(prevBlockHash);

		struct timespec endTime;
		clock_gettime(CLOCK_MONOTONIC, &endTime);

		double elapsedTimeSec = (endTime.tv_sec - startTime.tv_sec);
		elapsedTimeSec += (endTime.tv_nsec - startTime.tv_nsec) / 1000000000.0;

		// only the master adjusts the difficulty
		if (rank == 0) {
			if (elapsedTimeSec < avgBlockGenerationTime) {
				DIFFICULTY++;
				printf("Took %0.04f seconds to find block %d, SHORTER than expected so INCREASING difficulty to %d\n", elapsedTimeSec, i, DIFFICULTY);
			}
			else {
				DIFFICULTY--;
				printf("Took %0.04f seconds to find block %d, LONGER than expected so DECREASING difficulty to %d\n", elapsedTimeSec, i, DIFFICULTY);
			}

			for (int curRank = 1; curRank < numTasks; curRank++)
				MPI_Send(&DIFFICULTY, sizeof(int), MPI_BYTE, curRank, MPI_TAG_DIFFICULTY, MPI_COMM_WORLD);
		} else {
			MPI_Status status;
			MPI_Recv(&DIFFICULTY, sizeof(int), MPI_BYTE, 0, MPI_TAG_DIFFICULTY, MPI_COMM_WORLD, &status);
			printf("Received new difficulty of %d\n", DIFFICULTY);
		}
	}

	printf("Done\n");

	// done with MPI
	MPI_Finalize();
}

