#include "sha256.cuh"
#include <stdio.h>

extern "C" {
	#include "cpu_sha256.h"
}

__global__ void testSha()
{
    unsigned char outDat[SHA256_BLOCK_SIZE] = {0};
	char a = 'A';

    SHA256_CTX context1;
    sha256_init(&context1);
    sha256_update(&context1, (BYTE*)&a, 1);
    sha256_final(&context1, (BYTE*)outDat);

	printf("GPU Hash: %02X%02X%02X%02X\n", outDat[0], outDat[1], outDat[2], outDat[3]);
}


int main()
{
	unsigned char outDat[32];
	char a = 'A';
	CPU_SHA256_CTX context1;
	cpu_sha256_init(&context1);
	cpu_sha256_update(&context1, (BYTE*)&a, 1);
	cpu_sha256_final(&context1, (BYTE*)outDat);

	printf("CPU Hash: %02X%02X%02X%02X\n", outDat[0], outDat[1], outDat[2], outDat[3]);

	testSha<<<1,1>>>();
	cudaDeviceSynchronize();

	return 0;
}
