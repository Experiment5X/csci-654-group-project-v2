/*********************************************************************
* Filename:   sha256.h
* Author:     Brad Conte (brad AT bradconte.com)
* Copyright:
* Disclaimer: This code is presented "as is" without any guarantees.
* Details:    Defines the API for the corresponding SHA1 implementation.
*********************************************************************/

#ifndef CPU_SHA256_H
#define CPU_SHA256_H

/*************************** HEADER FILES ***************************/
#include <stddef.h>

/****************************** MACROS ******************************/
#define SHA256_BLOCK_SIZE 32            // SHA256 outputs a 32 byte digest

/**************************** DATA TYPES ****************************/
typedef unsigned char BYTE;             // 8-bit byte
typedef unsigned int  WORD;             // 32-bit word, change to "long" for 16-bit machines

typedef struct {
	BYTE data[64];
	WORD datalen;
	unsigned long long bitlen;
	WORD state[8];
} CPU_SHA256_CTX;

/*********************** FUNCTION DECLARATIONS **********************/
void cpu_sha256_init(CPU_SHA256_CTX *ctx);
void cpu_sha256_update(CPU_SHA256_CTX *ctx, const BYTE data[], size_t len);
void cpu_sha256_final(CPU_SHA256_CTX *ctx, BYTE hash[]);

#endif   // CPU_SHA256_H
