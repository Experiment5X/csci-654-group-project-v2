all: create_output # kernel cpu_sha256
	@nvcc -I/usr/local/include -L/usr/local/lib -lmpi -rdc=true src/mpi_test.cu src/cpu_sha256.c --maxrregcount=64 -o bin/mpi_test

sha: create_output # kernel cpu_sha256
	@nvcc src/stupid_sha.cu src/cpu_sha256.c -o bin/stupid_sha

create_output:
	@mkdir -p bin

clean:
	@rm -rf bin

run: all
	@mpirun --hostfile hosts ./bin/mpi_test

jrun:  # just run
	@mpirun --hostfile hosts ./bin/mpi_test
